# rsp-rules-drools
This is the rocket science platform rules engine implementation to expose Drools rules engine, this is the version without spark support.

## Local Development Machine Setup
### Install dmn-formulae-java8-1.1.jar into local Maven Repo
#### Run this from a command line
```
mvn install:install-file \
   -DlocalRepositoryPath=local-repo \
   -Dfile=lib/dmn-formulae-java8-1.1.jar \
   -DgroupId=com.signavio \
   -DartifactId=signavio-drl-support \
   -Dversion=1.0 \
   -Dpackaging=jar \
   -DgeneratePom=true
   ```
   
#### Using the above jar in pom.xml
   ```
   <dependency>
            <groupId>com.signavio</groupId>
            <artifactId>signavio-drl-support</artifactId>
            <version>1.0</version>
        </dependency>
   ```
## Generating the jar with all dependencies
``` 
mvn clean compile assembly:single
```
This will generate a new jar under target/
Look for a jar file ending with jar-with-dependencies.jar
## Running project in Eclipse or similar code editor
This project contain test class, when debugging the jar please setup your local run configuration with the following parameters:

Main class : com.rsp.RulesEngineTest

VM Arguments : -DCORE_ID=your_app_core_id_number -DAPP_ENVIRONMENT=your_app_env

Where **your_app_core_id_number** is a CORE ID (app id) you obtained from http://core/ and **your_app_env** is one of the dev/test/prod values.
