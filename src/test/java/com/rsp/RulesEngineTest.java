package com.rsp;

import com.rsp.drools.*;
import java.util.*;

import org.kie.api.KieServices;
import org.kie.api.runtime.KieSession;
import org.kie.api.runtime.KieContainer;
import org.kie.api.io.KieResources;
import org.kie.api.io.ResourceType;
import org.kie.api.builder.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class RulesEngineTest {
		
    public static final void main(String[] args) {
        try {
            
        	Logger logger = LoggerFactory.getLogger("test-logger");
        	
        	HashMap<String,String> structFields = new HashMap<String,String>();
        	structFields.put("ruleconfiguration", "{\"Object\":\"Property\","
        			+ "\"Package\":\"com.signavio.droolsexport_eefb20e532ee45dbbece6c13d16c2c89\","
        			+ "\"Wrapper\":\"Input\","
        			+ "\"CorrelationId\":\"123-0\","
        			+ "\"BusinessProcessId\":\"123-1\","
        			+ "\"BusinessActivityId\":\"123-2\","
        			+ "\"Ruleset\":\"jar_rulesengine_dataimpute\"}");
        	structFields.put("id", "1");
        	structFields.put("street", "29680 Delladonna Point");
        	structFields.put("city", "Atlanta");
        	structFields.put("state", "");
        	structFields.put("zip", "48000");
        	structFields.put("sqfootage", "3107");
        	structFields.put("bedrooms", "2");
        	structFields.put("propertyvalue", null);

        	Map<String,String> ret = new HashMap<String,String>();       	
        	ArrayList<String> runningLog = new ArrayList<String>();
        	
        	RunRules rr = new RunRules();
        	rr.setLogger(logger);
        	
        	ret = rr.call(structFields);
        	
        	for (Map.Entry<String, String> kv : ret.entrySet()) {
        		logger.info("RET IMPUTE: " + kv.getKey() + " - " + kv.getValue());
        	} 
        	
        	structFields = new HashMap<String,String>();
        	structFields.put("ruleconfiguration", "{\"Object\":\"Property\","
        			+ "\"Package\":\"com.signavio.droolsexport._e0d0806e72aa47a5922e560b9e0d4c34\","
        			+ "\"Wrapper\":\"Input\","
        			+ "\"CorrelationId\":\"1234-0\","
        			+ "\"BusinessProcessId\":\"1234-1\","
        			+ "\"BusinessActivityId\":\"1234-2\","
        			+ "\"Ruleset\":\"jar_rulesengine_dataquality\"}");
        	structFields.put("id", "1");
        	structFields.put("street", "29680 Delladonna Point");
        	structFields.put("city", "Atlanta");
        	structFields.put("state", "");
        	structFields.put("zip", "48000");
        	structFields.put("sqfootage", "3107");
        	structFields.put("bedrooms", "2");
        	structFields.put("propertyvalue", null);
        	
    		ret = rr.call(structFields);
        	
        	for (Map.Entry<String, String> kv : ret.entrySet()) {
        		logger.info("RET QUALITY: " + kv.getKey() + " - " + kv.getValue());
        	} 
        	
			//ret = rr.call(row )
        	/*
        	RulesEngine re = new RulesEngine(null);
            
        	// add objects to rules engine
        	re.objectsEmpty();
        	
        	Property a = new Property();
        	a.setState("MI");
        	a.setZip("48000");
        	re.objectsAdd(a);
        	   	     
        	re.runRules();
        	for (String s : re.getRulesLog()) {
        		runningLog.add(s);
        	}
        	
        	for (String s : re.getMetricsLog()) {
        		runningLog.add(s);
        	}
        	
        	// get the log of what happened
        	for (String s : runningLog) {
        		System.out.println("TRACE " + s);
        	}       	


        	// add objects to rules engine
        	re.objectsEmpty();
        	
        	a = new Property();
        	a.setState("MI");
        	re.objectsAdd(a);
        	
        	re.runRules();
        	for (String s : re.getRulesLog()) {
        		runningLog.add(s);
        	}
        	
        	// add objects to rules engine
        	re.objectsEmpty();
        	
        	a = new Property();
        	a.setState("");
        	re.objectsAdd(a);
        	
        	re.runRules();
        	for (String s : re.getRulesLog()) {
        		runningLog.add(s);
        	}
        	
        	for (String s : re.getMetricsLog()) {
        		runningLog.add(s);
        	}
        	
        	// get the log of what happened
        	for (String s : runningLog) {
        		System.out.println("TRACE " + s);
        	}       	
			*/
        	
        	
        } catch (Throwable t) {
            t.printStackTrace();
        }
    }

}

